# Admin package for Laravel

This package will install minimal API-driven Vuetify admin panel.

## Installation

You can install this package via composer using this command:

```bash
composer require "wai/adminify-api"
```

The package will automatically register itself.

## Upgrading

Please see [UPGRADING](UPGRADING.md) for details.

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Credits

- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
