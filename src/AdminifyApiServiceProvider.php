<?php

namespace Wai\AdminifyApi;

use Illuminate\Support\ServiceProvider;

class AdminifyApiServiceProvider extends ServiceProvider
{
    public function boot()
    {
    }

    public function register()
    {

    }
}
